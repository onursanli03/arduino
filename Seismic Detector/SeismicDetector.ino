//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Seismic detector on MMA7361
//
// Made by Onur Şanlı
// License: GPL 3.0

#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 7, 12, 11, 10, 9);
int x,y,z;
float gf;
float deltaG;
const float dif = 0.025;
const int buzzerPin = 5;
const int led = 13;

void setup(){
  pinMode(0, INPUT);
  pinMode(1, INPUT);
  pinMode(2, INPUT);
  pinMode(buzzerPin, OUTPUT);
  pinMode(led, OUTPUT);
  lcd.begin(16,2);
}

void get_g() {
  x = analogRead(A0);
  y = analogRead(A1);
  z = analogRead(A2);
  float Rx = (x*3.3/1023 - 1.65) / 0.8;
  float Ry = (y*3.3/1023 - 1.65) / 0.8;
  float Rz = (z*3.3/1023 - 1.65) / 0.8;
  gf = sqrt(Rx*Rx+Ry*Ry+Rz*Rz);
  deltaG = abs(gf-1);
  Serial.print("\nx: ");
  Serial.print(x);
  Serial.print(" \ty: ");
  Serial.print(y);
  Serial.print(" \tz: ");
  Serial.println(z);
  Serial.print("Gf: ");
  Serial.println(gf,6);
  Serial.print("deltaG: ");
  Serial.println(deltaG,6);
}

void lcd_info(){
  lcd.setCursor(0,1);
  lcd.print("Gf = ");
  lcd.print(gf,4);
  lcd.print(" G");
}

void loop(){
  get_g();
  lcd.clear();
  if (deltaG>dif){
    digitalWrite(led,HIGH);
    beep(25);
    lcd_Status("Danger!");
    lcd_info();
  }
  delay(100);
  digitalWrite(led, LOW);
}

void lcd_Status(char *msg) {
  lcd.setCursor(0, 0);
  lcd.print(msg);
}

void msgData(char *msg) {
  lcd.setCursor(0, 1);
  lcd.print(msg);
}

void beep(unsigned char delayms){
  analogWrite(buzzerPin, 20);
  delay(delayms);
  analogWrite(buzzerPin, 0);
  delay(delayms);
}
