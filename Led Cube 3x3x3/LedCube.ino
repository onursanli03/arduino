//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___ 
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __| 
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \ 
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/ 
// 
// LED Cube
// 
// Made by Onur Şanlı
// License: CC-BY-SA 3.0


int timer =500;
int h = HIGH;
int l = LOW;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
       
  pinMode(1, OUTPUT);     
  pinMode(2, OUTPUT);     
  pinMode(3, OUTPUT);     
  pinMode(4, OUTPUT);     
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT); // level 1 gnd
  pinMode(11, OUTPUT);// level 2 gnd
  pinMode(12, OUTPUT);  // level 3 gnd  

digitalWrite(10,h);
digitalWrite(11,h);
digitalWrite(12,h);
}

// the loop routine runs over and over again forever:
void loop() {
 
  digitalWrite(10,l);
        digitalWrite(9,h);
  		delay(timer);
        digitalWrite(9,l); 
 		delay(timer); 
 
 digitalWrite(11,l);
        digitalWrite(9,h);
  		delay(timer);
        digitalWrite(9,l); 
 		delay(timer); 
 
  digitalWrite(12,l);
        digitalWrite(9,h);
  		delay(timer);
        digitalWrite(9,l); 
 		delay(timer); 
 
  digitalWrite(12,l);
        digitalWrite(6,h);
  		delay(timer);
        digitalWrite(6,l); 
 		delay(timer); 

  digitalWrite(12,l);
        digitalWrite(3,h);
  		delay(timer);
        digitalWrite(3,l); 
 		delay(timer); 
  
  digitalWrite(11,l);
        digitalWrite(3,h);
  		delay(timer);
        digitalWrite(3,l); 
 		delay(timer); 
  
  digitalWrite(11,l);
        digitalWrite(6,h);
  		delay(timer);
        digitalWrite(6,l); 
 		delay(timer); 
  
  digitalWrite(10,l);
        digitalWrite(3,h);
  		delay(timer);
        digitalWrite(3,l); 
 		delay(timer); 
  
  digitalWrite(10,l);
        digitalWrite(6,h);
  		delay(timer);
        digitalWrite(6,l); 
 		delay(timer); 
 
  digitalWrite(10,l);
        digitalWrite(8,h);
  		delay(timer);
        digitalWrite(8,l); 
 		delay(timer); 
 
 digitalWrite(11,l);
        digitalWrite(8,h);
  		delay(timer);
        digitalWrite(8,l); 
 		delay(timer); 
 
  digitalWrite(12,l);
        digitalWrite(8,h);
  		delay(timer);
        digitalWrite(8,l); 
 		delay(timer); 
}
