//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___ 
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __| 
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \ 
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/ 
// 
// The Unnamed Circuit
// 
// Made by Onur Şanlı
// License: CC-BY-SA 3.0
// Downloaded from: https://circuits.io/circuits/2693654-the-unnamed-circuit

int redPin = 4;

int greenPin = 3;

int bluePin = 2;



void setup()

{

  pinMode(redPin, OUTPUT);

  pinMode(greenPin, OUTPUT);

  pinMode(bluePin, OUTPUT);

}



void loop()

{

  setColor(255, 0, 0);  // kırmızı

  delay(1000);

  setColor(0, 255, 0);  // yeşil

  delay(1000);

  setColor(0, 0, 255);  // mavi

  delay(1000);

  setColor(255, 255, 0);  // sarı

  delay(1000);

  setColor(80, 0, 80);  // mor

  delay(1000);

  setColor(0, 255, 255);  // aqua

  delay(1000);

}



void setColor(int red, int green, int blue)

{

  analogWrite(redPin, red);

  analogWrite(greenPin, green);

  analogWrite(bluePin, blue);

}
