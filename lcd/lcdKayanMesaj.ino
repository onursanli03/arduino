//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___ 
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __| 
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \ 
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/ 
// 
// The Unnamed Circuit
// 
// Made by Onur Şanlı
// License: CC-BY-SA 3.0
// Downloaded from: https://circuits.io/circuits/2693654-the-unnamed-circuit

#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  }
char message[4][16] = {"wtf","is","going","on"};
int a=0;
void loop() {
 for(int j=0;j<4;j++)
 {
    if(a==0)
    { 
     for(int i=0;i<16;i++)
      {
        lcd.setCursor(i, 0);
        lcd.print(message[j]);
        delay(300);
        lcd.setCursor(i, 0);
        lcd.print(' ');
        a=1;
      }
    }
    if (a==1)
    {
      for(int i=0;i<16;i++)
      {
        lcd.setCursor(i, 1);
        lcd.print(message[j]);
        lcd.setCursor(i, 0);
        lcd.print(message[j+1]);
        delay(300);
        lcd.setCursor(i, 1);
        lcd.print(' ');
         lcd.setCursor(i, 0);
        lcd.print(' ');
      }  
    }
}
a=0;
}
