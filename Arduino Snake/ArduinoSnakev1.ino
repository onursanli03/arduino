//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// ArduinoSnake v1.0
// 
// Made by Onur Şanlı
// License: CC-BY-SA 3.0


int st1=13,st2=12,st3=11,st4=10,st5=9,st6=8,ri1=2,ri2=3,ri3=4,ri4=5,ri5=6,ri6=7,upArr=A0,downArr=A1,leftArr=A2,rightArr=A3;
int stlpce[]={st1,st2,st3,st4,st5,st6};
int riadky[]={ri1,ri2,ri3,ri4,ri5,ri6};
int tlacidla[]={upArr,downArr,leftArr,rightArr};
int stavTlacidla(int tlacidlo);
int pocetStlaceni0=0,pocetStlaceni1=0,pocetStlaceni2=0,pocetStlaceni3=0;
int pocetStlaceniX[]={pocetStlaceni0,pocetStlaceni1,pocetStlaceni2,pocetStlaceni3};
void zobrazZnak(int riadok,int stlpec);
void specDelay(int time);
int j=3,k=3,counter0=0,l=0,counter1=0,counter2=0;
int stlpecPotrava[]={1,2,3,4};
int riadokPotrava[]={2,4,1,3};
void zobrazPotrava();
int stlpceHistory[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int riadkyHistory[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
void poleUpdate();
void driverFunction();

void setup(){
  for(int i=0;i<6;i++){
  	pinMode(stlpce[i],OUTPUT);
    digitalWrite(stlpce[i],LOW);
  	pinMode(riadky[i],OUTPUT);
    digitalWrite(riadky[i],HIGH);
  }
  Serial.begin(9600);
}

void loop(){
  specDelay(1);
  if(pocetStlaceni0==0 && pocetStlaceni1==0 && pocetStlaceni2==0 && pocetStlaceni3==0){
    Serial.write(0);
  }
   if(pocetStlaceniX[0]==1){
     Serial.write(2);
      while(pocetStlaceniX[0]==1){
       	specDelay(1);
        j--;
        if(j<0){
          j=5;
        }
        driverFunction();
      }
    }
  if(pocetStlaceniX[1]==1){
      Serial.write(2);
      while(pocetStlaceniX[1]==1){
       	specDelay(1);
        j++;
        if(j>5){
          j=0;
        }
        driverFunction();
      }
    }
  if(pocetStlaceniX[2]==1){
    Serial.write(2);
    while(pocetStlaceniX[2]==1){
       	specDelay(1);
        k--;
        if(k<0){
          k=5;
        }
        driverFunction();
      }
    }
  if(pocetStlaceniX[3]==1){
    Serial.write(2);
    while(pocetStlaceniX[3]==1){
       	specDelay(1);
        k++;
        if(k>5){
          k=0;
        }
        driverFunction();
      }
    }
}

int stavTlacidla(int tlacidlo){
  if(analogRead(tlacidlo)<300){
  	delay(20);
    if(analogRead(tlacidlo)==0){
      while(analogRead(tlacidlo)==0){}
      delay(20);
      return 1;
    }
  }
  return 0;
}

void zobrazZnak(int riadok,int stlpec){
  for(int i=0;i<6;i++){
    if(i==stlpec){
     digitalWrite(stlpce[i],HIGH);
    }
    for(int j=0;j<6;j++){
      if(j==riadok){
        digitalWrite(riadky[j],LOW);
      }
    }
  }
  delay(5);
  for(int i=0;i<6;i++){
    if(i==stlpec){
     digitalWrite(stlpce[i],LOW);
    }
    for(int j=0;j<6;j++){
      if(j==riadok){
        digitalWrite(riadky[j],HIGH);
      }
    }
  }
}

void specDelay(int time){
  for(int i=0;i<4;i++){
    if(stavTlacidla(tlacidla[i])==1){
      pocetStlaceniX[i]=1;
      for(int j=3;j>=0;j--){
        if(i!=j){
       		pocetStlaceniX[j]=0;
        }
      }
    }
  }
  delay(time);
}

void zobrazPotrava(){
  if(counter0>2){
   zobrazZnak(riadokPotrava[l],stlpecPotrava[l]);
   delay(5);
  }
}

void poleUpdate(){
  for(int i=35;i>0;i--){
   stlpceHistory[i]=stlpceHistory[i-1];
  }
  stlpceHistory[0]=k;
  for(int i=35;i>0;i--){
   riadkyHistory[i]=riadkyHistory[i-1];
  }
  riadkyHistory[0]=j;
  for(int i=1;i<=counter1;i++){
    if(stlpceHistory[i]==k && riadkyHistory[i]==j){
    	counter2=1;
    	Serial.write(counter2);
    }
  }
}

void driverFunction(){
  poleUpdate();
        for(int i=0;i<20;i++){
        	for(int m=0;m<=counter1;m++){
            zobrazZnak(riadkyHistory[m],stlpceHistory[m]);
          	specDelay(1);
         	zobrazPotrava();
            }
        }
        counter0++;
        if(stlpecPotrava[l]==k && riadokPotrava[l]==j){
         counter1++;
         l++;
         Serial.write(counter1+2);
         counter0=0;
         if(l>3){
           l=0;
          }
        }
        if(counter2==1){
          for(int i=0;i<6;i++){
  			digitalWrite(riadky[i],HIGH);
    		digitalWrite(stlpce[i],LOW);
          }
          for(int i=0;i<4;i++){
           pocetStlaceniX[i]=0;
          }
          for(int i=0;i<36;i++){
           stlpceHistory[i]=0;
           riadkyHistory[i]=0;
          }
           counter1=0;
           counter0=0;
           counter2=0;
        }
}
