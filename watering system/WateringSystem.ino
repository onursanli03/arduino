//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Watering System
//
// Made by Onur Şanlı
// License: CC-BY-SA 3.0


#include <Servo.h>
// include the library code for the LCD
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(9, 8, 4, 5, 6, 7);


#define triger 250
#define servoPosition0 10  //the position of the flower1, the range of the servo position is 10~160
#define triggerValue0 50  //the trigger value of the moisture, the minimum value is 0, means the moisture is 0, the soil is very dry. the maximum value is 1024.
#define wateringTime0 30  //the time for watering the flower1, millisecond. delay 35ms, uses about 70ML water

//Define the parameters of the flower2
#define servoPosition1 75  //the position of the flower2
#define triggerValue1 50  //the trigger value of the moisture
#define wateringTime1 30  //the time for watering the flower2,millisecond

//Define the parameters of the flower3
#define servoPosition2 120  //the position of the flower3
#define triggerValue2 50 //the trigger value of the moisture
#define wateringTime2 30  //the time for watering the flower3,millisecond

Servo horizontalServo;  // create servo object to control a servo
Servo verticalServo;

//Define the moisture sensor pins
const int analogInPin0 = A0;  //
const int analogInPin1 = A1;
const int analogInPin2 = A2;


//the value readed from each moisture sensor
int moistureValue0 = 0;
int moistureValue1 = 0;
int moistureValue2 = 0;

//the sum of the 30 times sampling
long int moistureSum0 = 0;   //we need to sampling the moisture 30 times and get its average value, this variable is used to store the sum of the 30 times sampled value
long int moistureSum1 = 0;
long int moistureSum2 = 0;

//Define the RGB LED control pins
/*int redPin = 11;
int greenPin = 9;
int bluePin = 10;*/

//Define the water pump contorl pins
const int pumpAnodePin =  2;      //pin 6 connect to the anode of the pump
//const int pumpCathodePin =  7;   //pin 7 connect to the cathode of the pump

void initPosition()//Init the position of the servos
{
  verticalServo.write(15);
  horizontalServo.write(25);
  delay(1000);
}

//Defines a color for the RGB LED
/*void setColor(int red, int green, int blue)
{
  #ifdef COMMON_ANODE
    red = 255 - red;
    green = 255 - green;
    blue = 255 - blue;
  #endif

  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);
}*/

void moistureSampling()// read the value of the soil moisture
{
  for(int i = 0; i < 30; i++)//samping 30 time within 3 seconds
  {
    moistureSum0 = moistureSum0 + analogRead(analogInPin0);
    moistureSum1 = moistureSum1 + analogRead(analogInPin1);
    moistureSum2 = moistureSum2 + analogRead(analogInPin2);
    delay(100);
  }
  moistureValue0 = moistureSum0 / 30;//get the average value
  moistureValue1 = moistureSum1 / 30;
  moistureValue2 = moistureSum2 / 30;

  // print the results to the serial monitor:
  Serial.print("Moisture0 = " );
  Serial.print(moistureValue0);
  Serial.print("\t Moisture1 = ");
  Serial.print(moistureValue1);
  Serial.print("\t Moisture2 = ");
  Serial.println(moistureValue2);
  Serial.println();

  moistureSum0 = 0;//reset the variable
  moistureSum1 = 0;
  moistureSum2 = 0;
  delay(2000);     //delay 4 seconds
}


void proWatering(int servoPosition, int wateringTime)  //sweep watering
{
  horizontalServo.write(servoPosition - 10);  // setting the servo to the position of the flower
  delay(1000); //waiting the servo go to right position
  digitalWrite(pumpAnodePin, HIGH);  //the pump start working
  //digitalWrite(pumpCathodePin, LOW);

  for(int vPos = 20; vPos < 45; vPos += 5)//
  {
    verticalServo.write(vPos);
    for(int hPos = servoPosition - 10; hPos <= servoPosition + 20; hPos += 1)  // goes from 0 degrees to 180 degrees //25,160
    {                                  // in steps of 1 degree
      horizontalServo.write(hPos);              // tell servo to go to position in variable 'pos'
      delay(wateringTime);                       // waits for the servo to reach the position
    }

    verticalServo.write(vPos+5);
    for(int hPos = servoPosition + 20; hPos >= servoPosition - 10; hPos -= 1)     // goes from 180 degrees to 0 degrees
    {
      horizontalServo.write(hPos);              // tell servo to go to position in variable 'pos'
      delay(wateringTime);                       // waits for the servo to reach the position
    }
  }

  for(int vPos = 45; vPos > 20; vPos -= 5)//
  {
    verticalServo.write(vPos);
    for(int hPos = servoPosition - 10; hPos <= servoPosition + 20; hPos += 1)  // goes from 0 degrees to 180 degrees //25,160
    {                                  // in steps of 1 degree
      horizontalServo.write(hPos);              // tell servo to go to position in variable 'pos'
      delay(wateringTime);                       // waits 15ms for the servo to reach the position
    }

     verticalServo.write(vPos-5);
    for(int hPos = servoPosition + 20; hPos >= servoPosition - 10; hPos-=1)     // goes from 180 degrees to 0 degrees
    {
      horizontalServo.write(hPos);              // tell servo to go to position in variable 'pos'
      delay(wateringTime);                       // waits 15ms for the servo to reach the position
    }
  }
  digitalWrite(pumpAnodePin, LOW);
  //digitalWrite(pumpCathodePin, LOW);
  delay(1000);
  initPosition();
}

void watering(int servoPosition, int wateringTime)
{
  Serial.println(servoPosition);
  horizontalServo.write(servoPosition);
  delay(1000);
  digitalWrite(pumpAnodePin, HIGH);
  delay(10000);
  //digitalWrite(pumpAnodePin, LOW);
  delay(3000);
}

void setup()
{
  Serial.begin(9600);
  horizontalServo.attach(12);  // attaches the horizontal servo on pin 8 to the servo object
  verticalServo.attach(13);  //attaches the vertical servo on pin 9 to the servo object
  initPosition();
  delay(500);
  pinMode(pumpAnodePin, OUTPUT);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  //pinMode(pumpCathodePin, OUTPUT);
  //setColor(255, 255, 0);  // yellow
}

void loop()
{
  // Print a message to the LCD.
  lcd.clear();
  lcd.print("Reading data...");
  delay(1000);
  moistureSampling();
  lcd.setCursor(0, 0);
  lcd.print("Humidity 1 value:");
  lcd.setCursor(0, 1);
  lcd.print(moistureValue0);
  delay(1500);
  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print("Humidity 2 value:");
  lcd.setCursor(0, 1);
  lcd.print(moistureValue1);
  delay(1500);
  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print("Humidity 3 value:");
  lcd.setCursor(0, 1);
  lcd.print(moistureValue2);
  delay(1500);
  lcd.clear();
  //setColor(0, 255, 0);  // green

  if(moistureValue0 < triggerValue0)
  {
    //setColor(255, 0, 0);  // red
    lcd.clear();
  	lcd.print("Watering");
    lcd.setCursor(0, 1);
    lcd.print("Letucces...");
  	proWatering(servoPosition0, wateringTime0);
    return;
   }
   if(moistureValue1 < triggerValue1)
  {
    //setColor(255, 0, 0);  // red
    lcd.clear();
  	lcd.print("Watering");
    lcd.setCursor(0, 1);
    lcd.print("Tomatoes...");
  	proWatering(servoPosition1, wateringTime1);
    return;
   }
   if(moistureValue2 < triggerValue2)
  {
    //setColor(255, 0, 0);  // red
    lcd.clear();
  	lcd.print("Watering");
    lcd.setCursor(0, 1);
    lcd.print("Onions...");
    proWatering(servoPosition2, wateringTime2);
    return;
   }
}
