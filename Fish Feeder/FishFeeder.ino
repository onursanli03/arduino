//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Fish Feeder
//
// Made by Onur ŞAnlı
// License: CC-BY-SA 3.0


#include <Servo.h>

Servo servo;  // create servo object

//////////////////////////////////////////////////////////////////
//                            SETTINGS
//////////////////////////////////////////////////////////////////
int feedPos = 90;  // Feed Position (degrees)
int relaxPos = 180; // Relax Position (degrees)

int feedBtn = 8;
int timeBtn = 7;
int timeDefault = 43200; // seconds (default: 12h)43200

int rgbRLed = 10;
int rgbGLed = 11;
int rgbBLed = 12;
int relayData = 2;

int feedLed = 4;
//////////////////////////////////////////////////////////////////

int time = 0;

void setup(){
 servo.attach(5);
 pinMode(rgbRLed, OUTPUT);
 pinMode(rgbGLed, OUTPUT);
 pinMode(rgbBLed, OUTPUT);

 pinMode(feedBtn, INPUT);
 pinMode(timeBtn, INPUT);

 pinMode(feedLed, OUTPUT);
 pinMode(relayData, OUTPUT);

 servo.write(relaxPos);
 delay(1000);
}
void loop(){

  // Check if feedBtn is pressed

  if(digitalRead(feedBtn)==HIGH){
   feed();
  }

  // Check if timeBtn is pressed

  if(digitalRead(timeBtn)==HIGH){
    switch(timeDefault){

      case 10: // 10 sec
         timeDefault = 43200; // 12 h
         break;

     case 86400: // 24 h
         timeDefault = 10; // 10 s
         break;

      case 64800: // 18 h
         timeDefault = 86400; // 24 h
         break;

       case 43200: // 12 h
         timeDefault = 64800; // 18 h
         break;

    } // Switch

    time = 0; // Reset time
  }

  // Turn on rgb led

  rgbLed();

  // Feed

  if(time==timeDefault){
    feed();
    time = 0; // Reset time
  }

  delay(1000); //Wait a second
  time++;     // Increment time
}

void rgbLed(){
  switch(timeDefault){

     case 86400: // 24 h
         digitalWrite(12, HIGH);
         digitalWrite(11, LOW);
         digitalWrite(10, LOW);
         break;

      case 64800: // 18 h
         digitalWrite(11, HIGH);
         digitalWrite(12, LOW);
         digitalWrite(10, LOW);
         break;

       case 43200: // 12 h
         digitalWrite(10, HIGH);
         digitalWrite(11, LOW);
         digitalWrite(12, LOW);
         break;

    case 10: // 10 sec
         digitalWrite(10, LOW);
         digitalWrite(11, HIGH);
         digitalWrite(12, HIGH);
         break;

    } // Switch
}

void feed(){

  digitalWrite(feedLed, HIGH);
  digitalWrite(relayData, HIGH);
  delay(500);

  servo.write(feedPos);
  delay(200);
  servo.write(feedPos-45);
  delay(200);
  servo.write(feedPos+45);
  delay(200);
  servo.write(feedPos);



  delay(1000);
  servo.write(relaxPos);
  delay(1000);

  digitalWrite(relayData, LOW);
  delay(500);
  digitalWrite(feedLed, LOW);
}
