//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Oh Christmas Tree
//
// Made by Onur Şanlı
// License: CC-BY-SA 3.0

// Used stupid names cause it didn't like simple variables
int turkey = 13;									// Red1
int chicken = 12;									// Blue1
int peacock = 11;									// Green1
int hawk = 10;										// Red2
int emu = 9;										// Blue2
int condor = 8;										// Green2
int eagle = 7;										// Red3
int crow = 6;										// Blue3
int satanicNightjar = 5;							// Green3
int star = 3;										// Star LEDs

long interval1 = 61;								// interval for first chain of LEDs (milliseconds)
long interval2 = 61;								// interval for second chain of LEDs (milliseconds)
long interval3 = 61;								// interval for third chain of LEDs (milliseconds)

int fadeValue=0;

void setup () {
    pinMode (turkey, OUTPUT);
    pinMode (chicken, OUTPUT);
    pinMode (peacock, OUTPUT);
    pinMode (hawk, OUTPUT);
    pinMode (emu, OUTPUT);
  	pinMode (condor, OUTPUT);
  	pinMode (eagle, OUTPUT);
  	pinMode (crow, OUTPUT);
  	pinMode (satanicNightjar, OUTPUT);
  	pinMode (star, OUTPUT);
}


void loop ()
{
	unsigned long m = millis();							// current time for RGBs
  	unsigned long n = millis();							// current time for star
	// Chain 1
	if ( m == 0 )
	{	digitalWrite (turkey, HIGH) ;					// White
		digitalWrite (chicken, HIGH) ;
		digitalWrite (peacock, HIGH) ;	}
	else if ( m % (interval1*5) == 0)
	{	digitalWrite (chicken, LOW) ;
      	digitalWrite (turkey, HIGH) ;	}				// Yellow
	/*else if ( m % (interval1*5) == 0 )
	{	digitalWrite (chicken, LOW) ;	}				// Green*/
	else if ( m % (interval1*4) == 0 )
	{	digitalWrite (peacock, HIGH) ;	}				// Cyan
	else if ( m % (interval1*3) == 0 )
	{	digitalWrite (turkey, LOW) ;	}				// Blue
	else if ( m % (interval1*2) == 0 )
	{	digitalWrite (chicken, HIGH) ;	}				// Magenta
	else if ( m % (interval1*1) == 0 )
	{	digitalWrite (chicken, LOW) ;					// Red
		digitalWrite (peacock, LOW) ;
	}

	// Chain 2
	if ( m == 0 )
	{	digitalWrite (hawk, HIGH) ;						// Yellow
		digitalWrite (emu, LOW) ;
		digitalWrite (condor, HIGH) ;	}
	else if ( m % (interval2*5) == 0 )
	{	digitalWrite (hawk, HIGH) ;		}				// Magenta
	else if ( m % (interval2*4) == 0 )
	{	digitalWrite (condor, LOW) ;	}				// Blue
	else if ( m % (interval2*3) == 0 )
	{	digitalWrite (hawk, LOW) ;
      	digitalWrite (condor, HIGH) ;
      	digitalWrite (emu, HIGH) ;		}				// Cyan
	/*else if ( m % (interval2*3) == 0 )
	{	digitalWrite (hawk, LOW) ;
		digitalWrite (condor, HIGH) ;	}				// Green*/
	else if ( m % (interval2*2) == 0 )
	{	digitalWrite (emu, LOW) ;						// Red
		digitalWrite (condor, LOW) ;	}
	else if ( m % (interval2*1) == 0)
	{	digitalWrite (emu, HIGH) ;		}				// White

	// Chain 3
	if ( m == 0 )
	{	digitalWrite (crow, HIGH);						// Blue
		digitalWrite (eagle, LOW) ;
		digitalWrite (satanicNightjar, LOW) ;	}
	else if ( m % (interval3*5) == 0 )
	{	digitalWrite (crow, HIGH) ;				}		// Magenta
	else if ( m % (interval3*4) == 0 )
	{	digitalWrite (satanicNightjar, LOW) ;			// Red
		digitalWrite (crow, LOW) ;				}
	else if ( m % (interval3*3) == 0 )
	{	digitalWrite (crow, HIGH) ;				}		// White
	else if ( m % (interval3*2) == 0 )
	{	digitalWrite (crow, LOW) ;
      	digitalWrite (eagle, HIGH) ;			}		// Yellow
	/*else if ( m % (interval3*2) == 0 )
	{	digitalWrite (crow, LOW) ;				}		// Green*/
	else if ( m % (interval3*1) == 0 )
	{	digitalWrite (satanicNightjar, HIGH) ;	}		// Cyan
	else
		m=0;											// Shake the magic 8 ball

  	// Star
	fadeValue = 150+105*sin(2*PI/500*n) ;
  	analogWrite (star, fadeValue);

}
