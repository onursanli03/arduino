//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Door Lock Digital
//
// Made by Onur Şanlı
// License: CC-BY-SA 3.0


#include <Wire.h>
#include<LiquidCrystal.h>
#include <Keypad.h>
#include <Servo.h>

//constants for LEDs
int greenLED = 11;
int redLED = 12;
int piezopin = 1;

//set our code
char* lockCode = "1921";
int currentPosition = 0;

//servo
Servo ServoMotor;  // create servo object to control a servo
int pos = 0; // variable to store the servo position

//define the keypad
const byte rows = 4;
const byte cols = 3;

char keys[rows][cols] = {
                          {'1','2','3'},
                          {'4','5','6'},
                          {'7','8','9'},
                          {'*','0','#'}
                                        };

byte rowPins[rows] = {A0, A1, A2, A3};
byte colPins[cols] = {A4, A5, 2};

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, rows, cols);
LiquidCrystal lcd(5,6,7,8,9,10);

void setup()
{
  lcd.begin(16, 2);
  ServoMotor.attach(3);  // attaches the servo on pin 3 to the servo object
  Serial.begin(9600);
  pinMode(piezopin, OUTPUT);

  displayCodeEntryScreen();

  //setup and turn off both LEDs
  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED, LOW);

}

void loop()
{
  char key = keypad.getKey();

  if (int(key) != 0) {
    lcd.setCursor(7,1);
    lcd.print("    ");
    lcd.setCursor(7,1);

    for (int l=0; l<=currentPosition; ++l)
    {
      lcd.print("*");
      tone(piezopin, 1740, 300);
    }

    if (key == lockCode[currentPosition])
      {
        ++currentPosition;
        if (currentPosition == 4)
        {
          unlockDoor();
          currentPosition = 0;
        }
      }
      else
      {
        invalidCode();
        currentPosition = 0;
      }

  }
}

void invalidCode()
{
  digitalWrite(redLED, HIGH);
  clearScreen();
  lcd.setCursor(0,0);
  lcd.print("* ACCESS DENIED **");
  lcd.setCursor(0,1);
  lcd.print("* INVALID CODE **");
  tone(piezopin, 1540, 1000);
  delay(3000);
  digitalWrite(redLED, LOW);
  digitalWrite(redLED, HIGH);
  displayCodeEntryScreen();
}

void unlockDoor()
{
  digitalWrite(greenLED, HIGH);
  clearScreen();
  lcd.setCursor(0,0);
  lcd.print(" ACCESS GRANTED ");
  lcd.setCursor(0,1);
  lcd.print("* !.WELCOME.!  *");
  ServoMotor.write(140);
  digitalWrite(redLED, LOW);
  tone(piezopin, 440, 1000);
  delay(600);

  //add any code to unlock the door here
  delay(5000);
  digitalWrite(greenLED, LOW);
  digitalWrite(redLED, HIGH);
  displayCodeEntryScreen();
}

void displayCodeEntryScreen()
{
  clearScreen();
  lcd.setCursor(0,0);
  lcd.print(".....WELCOME....");
  lcd.setCursor(0,1);
  lcd.print("Code:");
  ServoMotor.write(0);
}

void clearScreen()
{
  lcd.setCursor(0,0);
  lcd.print("                    ");
  lcd.setCursor(0,1);
  lcd.print("                    ");

}
