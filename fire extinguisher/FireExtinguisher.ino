//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// LM35 y Sensor Humedad con Motores
//
// Made by Onur Şanlı
// License: CC-BY-SA 3.0


int dato1;
int dato2;
float voltios;
float temperaturaC;
int alarma=7;
int techo=8;
int ventilador=9;

void ControlAlarma()
{
  if(temperaturaC<36 & dato2>850)
  {
  digitalWrite(alarma,LOW);
  }

  if(temperaturaC>=36)
  {
    digitalWrite(alarma,HIGH);
  }

  if(dato2<=850)
  {
    digitalWrite(alarma,HIGH);
  }
}

void ControlTecho()
{
  if(temperaturaC>=36)
  {
    digitalWrite(techo,HIGH);
  }
  else
  {
    digitalWrite(techo,LOW);
  }
}

void ControlVentilador()
{

  if(temperaturaC>=36)
  {
    digitalWrite(ventilador,HIGH);
  }
  else
  {
    digitalWrite(ventilador,LOW);
  }

}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(alarma,OUTPUT);
  pinMode(techo,OUTPUT);
  pinMode(ventilador,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  dato1 = analogRead(A0);
  voltios = (dato1 / 1023.0) * 5;
  temperaturaC = voltios / 10e-3;
  Serial.print(temperaturaC);
  Serial.println(" C");
  delay(300);

  Serial.println("");

  dato2 = analogRead(A1);
  Serial.println(dato2);
  delay(300);


  ControlAlarma();
  ControlVentilador();
  ControlTecho();


}
