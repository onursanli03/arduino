#include<Servo.h>


#define echoPin1 12
#define trigPin1 13
#define echoPin2 6
#define trigPin2 5
#define ledPinRed 8
#define ledPinBlue 9
#define ledPinGreen 10
#define ledPinYellow 11



Servo srv;

int maxRange = 200;
int minRange = 0;



void setup() {

  pinMode(trigPin1, OUTPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin1, INPUT);
  pinMode(echoPin2, INPUT);
  pinMode(ledPinRed , OUTPUT);
  pinMode(ledPinBlue , OUTPUT);
  pinMode(ledPinGreen , OUTPUT);
  pinMode(ledPinYellow , OUTPUT);
  

  srv.attach(7);
  
  Serial.begin(9600);
    
}

void loop() {

 // int measurement = range(maxRange,minRange,trigPin1,echoPin1);
 // int measurement2 = range(maxRange,minRange,trigPin2,echoPin2);
  //rangeControl(measurement);
  //rangeControl(measurement2);
  //srv.write(0);
  calculateDistance(echoPin1,trigPin1);  
  calculateDistance(echoPin2,trigPin2);  
  //Serial.print(measurement);
 // Serial.print(measurement2);
  
}

void calculateDistance(int echo, int trigger){
  long duration, distance;
 
  digitalWrite(trigger, LOW);              
  delayMicroseconds(2);                   
  digitalWrite(trigger, HIGH);             
 
  delayMicroseconds(10);                 
  digitalWrite(trigger, LOW);              
 
  duration = pulseIn(echo, HIGH);   
   
 
  distance = (duration/2) / 29.1;        

 
  if (distance >= 200 || distance <= 0){
  Serial.println("Out of range");    
       
  }
  else {
  if(distance > 0 && distance <= 12 ){
   digitalWrite(ledPinRed , HIGH);
    delay(500);
    digitalWrite(ledPinRed , LOW);
    srv.write(45);
    delay(1000);
  }else if(distance > 12 && distance <= 24){
    digitalWrite(ledPinBlue , HIGH);
    delay(500);
    digitalWrite(ledPinBlue , LOW);
    srv.write(75);
    
    delay(1000);
  }else if(distance > 24 && distance <= 50 ){
    digitalWrite(ledPinGreen , HIGH);
    delay(500);
    digitalWrite(ledPinGreen , LOW);
    srv.write(105);
    delay(1000);
  }else if(distance > 50){
    digitalWrite(ledPinYellow , HIGH);
    delay(500);
    digitalWrite(ledPinYellow , LOW);
    srv.write(135);
    delay(1000);
  }
            
    
  Serial.print(distance);  

  Serial.println(" cm");
  }
}


