//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Parking Sensor System
//
// Made by Onur Şanlı
// License: CC-BY-SA 3.0

#include<LiquidCrystal.h>
#define buzzer 6
#define ultrasonic 7
#define RPIN 8
#define GPIN 9

LiquidCrystal lcd(12,11,5,4,3,2);

void setup()
{
  Serial.begin(9600);
  pinMode(RPIN,OUTPUT);
  pinMode(GPIN,OUTPUT);
  pinMode(buzzer,OUTPUT);

  lcd.begin(16,2);
}

void loop()
{
  lcd.setCursor(0,0);
  long distance,cm,duration;

 //Triggering ultrasonic sensor

  pinMode(ultrasonic,OUTPUT);
  digitalWrite(ultrasonic,LOW);
  delayMicroseconds(2);
  digitalWrite(ultrasonic,HIGH);
  delayMicroseconds(5);
  digitalWrite(ultrasonic,LOW);

 //Reading from ultrasonic sensor

  pinMode(ultrasonic,INPUT);
  duration=pulseIn(ultrasonic,HIGH);

 //convert the time into a distance

  cm = microsecondsToCentimeters(duration);
  Serial.println(cm);
  delay(100);

  distance=cm;
  lcd.print("Distance = ");
  lcd.print(distance);
  Serial.println(distance);


  if(distance<=150)
  {
   digitalWrite(RPIN,HIGH);
   digitalWrite(GPIN,LOW);     // red
   digitalWrite(buzzer,HIGH);
   delay(100);
  }

  else if(distance>150)
  {
   digitalWrite(RPIN, LOW);
   digitalWrite(GPIN,HIGH);    //green
   digitalWrite(buzzer,LOW);
   delay(100);
  }
}

long microsecondsToCentimeters(long microseconds)
  {
  return microseconds / 29 / 2;
  }
