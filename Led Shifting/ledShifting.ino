//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Led Shifting
//
// Made by Onur Şanlı
// License: CC-BY-SA 3.0



int latchPin = 8;

int clockPin = 12;

int dataPin = 11;


byte ledStatus;
void setup() {

  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {

  ledStatus = 0;
  for (int i = 0; i < 8; i++) {
    Serial.println(ledStatus);

    ledStatus = (ledStatus << 1) | 1;

    Serial.println(ledStatus);
    Serial.println("--------");


    digitalWrite(latchPin, LOW);


    shiftOut(dataPin, clockPin, MSBFIRST, ledStatus);

    digitalWrite(latchPin, HIGH);



    delay(500);
  }


  for (int i = 0;i<8;i++) {
    ledStatus <<= 1; 
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, MSBFIRST, ledStatus);
    digitalWrite(latchPin, HIGH);
    delay(500);
  }
}
