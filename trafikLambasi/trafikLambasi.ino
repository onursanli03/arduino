#define arabaLedRed 8
#define arabaLedYellow 9
#define arabaLedGreen 10

#define yayaLedRed 7
#define yayaLedGreen 6

#define buttonPin 5

int crossTime = 5000;

unsigned long changeTime;

void setup() {
  

  pinMode(arabaLedRed, OUTPUT);
  pinMode(arabaLedYellow, OUTPUT);
  pinMode(arabaLedGreen, OUTPUT);
  pinMode(yayaLedRed, OUTPUT);
  pinMode(yayaLedGreen, OUTPUT);
  pinMode(buttonPin, INPUT);

  digitalWrite(arabaLedGreen,HIGH);
  digitalWrite(yayaLedRed,HIGH);


}

void loop() {

  int state = digitalRead(buttonPin);

  if(state == HIGH && (millis()-changeTime) > 5000){
    changeLights();
  }

  

}

void changeLights() {
digitalWrite(arabaLedGreen, LOW);
digitalWrite(arabaLedYellow, HIGH);
delay(2000); 
digitalWrite(arabaLedYellow, LOW); 
digitalWrite(arabaLedRed, HIGH); 
delay(1000); 
digitalWrite(yayaLedRed, LOW); 
digitalWrite(yayaLedGreen, HIGH); 
delay(crossTime); 

for (int x=0; x<10; x++) {
digitalWrite(yayaLedGreen, HIGH);
delay(250);
digitalWrite(yayaLedGreen, LOW);
delay(250);
}

digitalWrite(yayaLedRed, HIGH);
delay(500);
digitalWrite(arabaLedYellow, HIGH); 
digitalWrite(arabaLedYellow, LOW); 
delay(1000);
digitalWrite(arabaLedGreen, HIGH);
digitalWrite(arabaLedRed, LOW); 


changeTime = millis();

}
