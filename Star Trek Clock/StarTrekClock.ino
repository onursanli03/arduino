//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___ 
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __| 
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \ 
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/ 
// 
// Star Trek Clock
// 
// Made by Onur Şanlı
// License: CC-BY-SA 3.0

#include <LiquidCrystal.h>

// Left LCD
LiquidCrystal lcd1(7, 8, 9, 10, 11, 12);

// Right LCD
LiquidCrystal lcd2(1, 2, 3, 4, 5, 6);

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
	#include <avr/power.h>
#endif

// NeoPixel Ring 24
#define PIN 13

#define NUMPIXELS 24

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 50;

long previousMillis = 0;
long interval = 950;
int seconds = 0;
int hour = 0;

int hourButtonState = 0;
int minButtonState = 0;

byte full[8] = {
  B11111, B11111, B11111, B11111, B11111, B11111, B11111, B11111,
};

byte halfL[8] = {
  B11111, B11111, B11111, B11111, B00000, B00000, B00000, B00000,
};

byte halfR[8] = {
  B00000, B00000, B00000, B00000, B11111, B11111, B11111, B11111,
};

// the setup routine runs once when you press reset:
void setup() {
  
  // set button pins
  pinMode(14, INPUT); // set minute
  pinMode(15, INPUT); // set hour
  
  // make custom characters
  lcd1.createChar(0, halfR);
  lcd1.createChar(1, halfL);
  lcd1.createChar(2, full);
  lcd2.createChar(0, halfR);
  lcd2.createChar(1, halfL);
  lcd2.createChar(2, full);

  // set LCD1 size
  lcd1.begin(16,2);
  
  // set LCD2 size
  lcd2.begin(16, 2);
  
  // pixel ring
  pixels.begin();
  
  // initial setup
  lcd1.clear();
  num0lcd1();
  lcd2.clear();
  num0lcd2();
      
  //pixels.setPixelColor(17, pixels.Color(0,150,0));
  //pixels.setPixelColor(18, pixels.Color(0,150,0));
  pixels.show();
}

// the loop routine runs over and over again forever:
void loop() {
  
  hourButtonState = digitalRead(15);
  minButtonState = digitalRead(14);
  
  if (hourButtonState == HIGH) {
    hour++;
    runClock ();
  }
  if (minButtonState == HIGH) {
    seconds++;
    runClock ();
  }
  
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    if (seconds == 59) {
    	seconds = 0;
      	if (hour == 11) {
       		hour = 0; 
      	} else {
      		hour ++;
      	}
    } else {
      seconds ++;
    }
    previousMillis = currentMillis;
    runClock ();
  }
  delay(10);
}

void runClock () {
  
  if (seconds == 0) {
    lcd1.clear();
    num0lcd1();
    lcd2.clear();
    num0lcd2();
  	}
  	if (seconds == 1 || seconds == 11 || seconds == 21 || seconds == 31 || seconds == 41 || seconds == 51) {
    	lcd1.clear();
    	num1lcd1();
  	}
  	if (seconds == 2 || seconds == 12 || seconds == 22 || seconds == 32 || seconds == 42 || seconds == 52) {
    	lcd1.clear();
    	num2lcd1();
  	}
  	if (seconds == 3 || seconds == 13 || seconds == 23 || seconds == 33 || seconds == 43 || seconds == 53) {
    	lcd1.clear();
    	num3lcd1();
  	}
  	if (seconds == 4 || seconds == 14 || seconds == 24 || seconds == 34 || seconds == 44 || seconds == 54) {
    	lcd1.clear();
    	num4lcd1();
  	}
  	if (seconds == 5 || seconds == 15 || seconds == 25 || seconds == 35 || seconds == 45 || seconds == 55) {
    	lcd1.clear();
    	num5lcd1();
  	}
  	if (seconds == 6 || seconds == 16 || seconds == 26 || seconds == 36 || seconds == 46 || seconds == 56) {
    	lcd1.clear();
    	num6lcd1();
  	}
  	if (seconds == 7 || seconds == 17 || seconds == 27 || seconds == 37 || seconds == 47 || seconds == 57) {
    	lcd1.clear();
    	num7lcd1();
  	}
  	if (seconds == 8 || seconds == 18 || seconds == 28 || seconds == 38 || seconds == 48 || seconds == 58) {
    	lcd1.clear();
    	num8lcd1();
  	}
  	if (seconds == 9 || seconds == 19 || seconds == 29 || seconds == 39 || seconds == 49 || seconds == 59) {
    	lcd1.clear();
    	num9lcd1();
  	}
  	if (seconds == 10) {
    	lcd1.clear();
    	num0lcd1();
    	lcd2.clear();
    	num1lcd2();
  	}
  	if (seconds == 20) {
    	lcd1.clear();
    	num0lcd1();
    	lcd2.clear();
    	num2lcd2();
  	}
  	if (seconds == 30) {
    	lcd1.clear();
    	num0lcd1();
    	lcd2.clear();
    	num3lcd2();
  	}
  	if (seconds == 40) {
    	lcd1.clear();
    	num0lcd1();
    	lcd2.clear();
    	num4lcd2();
  	}
  	if (seconds == 50) {
    	lcd1.clear();
    	num0lcd1();
    	lcd2.clear();
    	num5lcd2();
  	}
    if (hour == 0) {
      	pixels.setPixelColor(15, pixels.Color(0,0,0));
    	pixels.setPixelColor(16, pixels.Color(0,0,0));
      	pixels.setPixelColor(17, pixels.Color(0,150,0));
    	pixels.setPixelColor(18, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 1) {
      	pixels.setPixelColor(17, pixels.Color(0,0,0));
    	pixels.setPixelColor(18, pixels.Color(0,0,0));
    	pixels.setPixelColor(19, pixels.Color(0,150,0));
    	pixels.setPixelColor(20, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 2) {
      	pixels.setPixelColor(19, pixels.Color(0,0,0));
    	pixels.setPixelColor(20, pixels.Color(0,0,0));
      	pixels.setPixelColor(21, pixels.Color(0,150,0));
    	pixels.setPixelColor(22, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 3) {
      	pixels.setPixelColor(21, pixels.Color(0,0,0));
    	pixels.setPixelColor(22, pixels.Color(0,0,0));	
      	pixels.setPixelColor(23, pixels.Color(0,150,0));
    	pixels.setPixelColor(0, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 4) {
      	pixels.setPixelColor(23, pixels.Color(0,0,0));
    	pixels.setPixelColor(0, pixels.Color(0,0,0));
      	pixels.setPixelColor(1, pixels.Color(0,150,0));
    	pixels.setPixelColor(2, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 5) {
      	pixels.setPixelColor(1, pixels.Color(0,0,0));
    	pixels.setPixelColor(2, pixels.Color(0,0,0));
      	pixels.setPixelColor(3, pixels.Color(0,150,0));
    	pixels.setPixelColor(4, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 6) {
      	pixels.setPixelColor(3, pixels.Color(0,0,0));
    	pixels.setPixelColor(4, pixels.Color(0,0,0));
      	pixels.setPixelColor(5, pixels.Color(0,150,0));
    	pixels.setPixelColor(6, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 7) {
      	pixels.setPixelColor(5, pixels.Color(0,0,0));
    	pixels.setPixelColor(6, pixels.Color(0,0,0));
      	pixels.setPixelColor(7, pixels.Color(0,150,0));
    	pixels.setPixelColor(8, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 8) {
      	pixels.setPixelColor(7, pixels.Color(0,0,0));
    	pixels.setPixelColor(8, pixels.Color(0,0,0));
      	pixels.setPixelColor(9, pixels.Color(0,150,0));
    	pixels.setPixelColor(10, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 9) {
      	pixels.setPixelColor(9, pixels.Color(0,0,0));
    	pixels.setPixelColor(10, pixels.Color(0,0,0));
      	pixels.setPixelColor(11, pixels.Color(0,150,0));
    	pixels.setPixelColor(12, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 10) {
      	pixels.setPixelColor(11, pixels.Color(0,0,0));
    	pixels.setPixelColor(12, pixels.Color(0,0,0));
      	pixels.setPixelColor(13, pixels.Color(0,150,0));
    	pixels.setPixelColor(14, pixels.Color(0,150,0));
    	pixels.show();
    }
    if (hour == 11) {
      	pixels.setPixelColor(13, pixels.Color(0,0,0));
    	pixels.setPixelColor(14, pixels.Color(0,0,0));
      	pixels.setPixelColor(15, pixels.Color(0,150,0));
    	pixels.setPixelColor(16, pixels.Color(0,150,0));
    	pixels.show();
    }
}

void num0lcd1 () {
  
  lcd1.setCursor(0,0);
  lcd1.write(byte(2)); // full
  lcd1.setCursor(0, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(1, 1);
  lcd1.write(byte(0)); // half right
  lcd1.setCursor(1, 0);
  lcd1.write(byte(1)); // half left
  lcd1.setCursor(2, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(2, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(3, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(3, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(4, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(4, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(5, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(5, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(6, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(6, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(7, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(7, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(8, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(8, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(9, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(9, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(10, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(10, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(11, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(11, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(12, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(12, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(13, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(13, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(14, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(14, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(15, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(15, 0);
  lcd1.write(byte(2));
}

void num1lcd1 () {
  
  lcd1.setCursor(0,1);
  lcd1.write(byte(0));
  lcd1.setCursor(1,1);
  lcd1.write(byte(0));
  lcd1.setCursor(2,1);
  lcd1.write(byte(0));
  lcd1.setCursor(3,1);
  lcd1.write(byte(0));
  lcd1.setCursor(4,1);
  lcd1.write(byte(0));
  lcd1.setCursor(5,1);
  lcd1.write(byte(0));
  lcd1.setCursor(6,1);
  lcd1.write(byte(0));
  lcd1.setCursor(7,1);
  lcd1.write(byte(0));
  lcd1.setCursor(8,1);
  lcd1.write(byte(0));
  lcd1.setCursor(9,1);
  lcd1.write(byte(0));
  lcd1.setCursor(10,1);
  lcd1.write(byte(0));
  lcd1.setCursor(11,1);
  lcd1.write(byte(0));
  lcd1.setCursor(12,1);
  lcd1.write(byte(0));
  lcd1.setCursor(13,1);
  lcd1.write(byte(0));
  lcd1.setCursor(14,1);
  lcd1.write(byte(0));
  lcd1.setCursor(15,1);
  lcd1.write(byte(0));
}

void num2lcd1 () {
  
  lcd1.setCursor(0,0);
  lcd1.write(byte(2)); // full
  lcd1.setCursor(0, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(1, 0);
  lcd1.write(byte(1)); // half left
  lcd1.setCursor(2, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(3, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(4, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(5, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(6, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(7, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(7, 0);
  lcd1.write(byte(2));
  lcd1.setCursor(8, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(9, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(10, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(11, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(12, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(13, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(14, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(15, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(15, 0);
  lcd1.write(byte(2));
}

void num3lcd1 () {
  
  lcd1.setCursor(0,0);
  lcd1.write(byte(2)); // full
  lcd1.setCursor(0, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(1, 1);
  lcd1.write(byte(0)); // half right
  lcd1.setCursor(2, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(3, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(4, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(5, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(6, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(7, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(7, 0);
  lcd1.write(byte(2));
  lcd1.setCursor(8, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(9, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(10, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(11, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(12, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(13, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(14, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(15, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(15, 0);
  lcd1.write(byte(2));
}

void num4lcd1 () {
  
 lcd1.setCursor(0, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(1, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(2, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(3, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(4, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(5, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(6, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(7, 1);
 lcd1.write(byte(2));
 lcd1.setCursor(7, 0);
 lcd1.write(byte(2));
 lcd1.setCursor(8, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(8, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(9, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(9, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(10, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(10, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(11, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(11, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(12, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(12, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(13, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(13, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(14, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(14, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(15, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(15, 1);
 lcd1.write(byte(0));
}

void num5lcd1 () {
  
  lcd1.setCursor(0,0);
  lcd1.write(byte(2)); // full
  lcd1.setCursor(0, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(1, 1);
  lcd1.write(byte(0)); // half right
  lcd1.setCursor(2, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(3, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(4, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(5, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(6, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(7, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(7, 0);
  lcd1.write(byte(2));
  lcd1.setCursor(8, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(9, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(10, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(11, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(12, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(13, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(14, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(15, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(15, 0);
  lcd1.write(byte(2));
}

void num6lcd1 () {
  
  lcd1.setCursor(0,0);
  lcd1.write(byte(2)); // full
  lcd1.setCursor(0, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(1, 1);
  lcd1.write(byte(0)); // half right
  lcd1.setCursor(1, 0);
  lcd1.write(byte(1)); // half left
  lcd1.setCursor(2, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(2, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(3, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(3, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(4, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(4, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(5, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(5, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(6, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(6, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(7, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(7, 0);
  lcd1.write(byte(2));
  lcd1.setCursor(8, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(9, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(10, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(11, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(12, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(13, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(14, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(15, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(15, 0);
  lcd1.write(byte(2));
}

void num7lcd1 () {
  
  lcd1.setCursor(0,1);
  lcd1.write(byte(0));
  lcd1.setCursor(1,1);
  lcd1.write(byte(0));
  lcd1.setCursor(2,1);
  lcd1.write(byte(0));
  lcd1.setCursor(3,1);
  lcd1.write(byte(0));
  lcd1.setCursor(4,1);
  lcd1.write(byte(0));
  lcd1.setCursor(5,1);
  lcd1.write(byte(0));
  lcd1.setCursor(6,1);
  lcd1.write(byte(0));
  lcd1.setCursor(7,1);
  lcd1.write(byte(0));
  lcd1.setCursor(8,1);
  lcd1.write(byte(0));
  lcd1.setCursor(9,1);
  lcd1.write(byte(0));
  lcd1.setCursor(10,1);
  lcd1.write(byte(0));
  lcd1.setCursor(11,1);
  lcd1.write(byte(0));
  lcd1.setCursor(12,1);
  lcd1.write(byte(0));
  lcd1.setCursor(13,1);
  lcd1.write(byte(0));
  lcd1.setCursor(14,1);
  lcd1.write(byte(0));
  lcd1.setCursor(15,1);
  lcd1.write(byte(2));
  lcd1.setCursor(15,0);
  lcd1.write(byte(2));
}

void num8lcd1 () {
  
  lcd1.setCursor(0,0);
  lcd1.write(byte(2)); // full
  lcd1.setCursor(0, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(1, 1);
  lcd1.write(byte(0)); // half right
  lcd1.setCursor(1, 0);
  lcd1.write(byte(1)); // half left
  lcd1.setCursor(2, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(2, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(3, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(3, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(4, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(4, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(5, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(5, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(6, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(6, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(7, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(7, 0);
  lcd1.write(byte(2));
  lcd1.setCursor(8, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(8, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(9, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(9, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(10, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(10, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(11, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(11, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(12, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(12, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(13, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(13, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(14, 1);
  lcd1.write(byte(0));
  lcd1.setCursor(14, 0);
  lcd1.write(byte(1));
  lcd1.setCursor(15, 1);
  lcd1.write(byte(2));
  lcd1.setCursor(15, 0);
  lcd1.write(byte(2));
}

void num9lcd1 () {
  
 lcd1.setCursor(0, 0);
 lcd1.write(byte(2));
 lcd1.setCursor(0, 1);
 lcd1.write(byte(2));
 lcd1.setCursor(1, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(2, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(3, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(4, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(5, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(6, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(7, 1);
 lcd1.write(byte(2));
 lcd1.setCursor(7, 0);
 lcd1.write(byte(2));
 lcd1.setCursor(8, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(8, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(9, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(9, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(10, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(10, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(11, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(11, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(12, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(12, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(13, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(13, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(14, 0);
 lcd1.write(byte(1));
 lcd1.setCursor(14, 1);
 lcd1.write(byte(0));
 lcd1.setCursor(15, 0);
 lcd1.write(byte(2));
 lcd1.setCursor(15, 1);
 lcd1.write(byte(2));
}

void num0lcd2 () {
  
  lcd2.setCursor(0,0);
  lcd2.write(byte(2)); // full
  lcd2.setCursor(0, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(1, 1);
  lcd2.write(byte(0)); // half right
  lcd2.setCursor(1, 0);
  lcd2.write(byte(1)); // half left
  lcd2.setCursor(2, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(2, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(3, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(3, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(4, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(4, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(5, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(5, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(6, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(6, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(7, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(7, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(8, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(8, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(9, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(9, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(10, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(10, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(11, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(11, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(12, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(12, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(13, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(13, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(14, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(14, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(15, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(15, 0);
  lcd2.write(byte(2));
}

void num1lcd2 () {
  
  lcd2.setCursor(0,1);
  lcd2.write(byte(0));
  lcd2.setCursor(1,1);
  lcd2.write(byte(0));
  lcd2.setCursor(2,1);
  lcd2.write(byte(0));
  lcd2.setCursor(3,1);
  lcd2.write(byte(0));
  lcd2.setCursor(4,1);
  lcd2.write(byte(0));
  lcd2.setCursor(5,1);
  lcd2.write(byte(0));
  lcd2.setCursor(6,1);
  lcd2.write(byte(0));
  lcd2.setCursor(7,1);
  lcd2.write(byte(0));
  lcd2.setCursor(8,1);
  lcd2.write(byte(0));
  lcd2.setCursor(9,1);
  lcd2.write(byte(0));
  lcd2.setCursor(10,1);
  lcd2.write(byte(0));
  lcd2.setCursor(11,1);
  lcd2.write(byte(0));
  lcd2.setCursor(12,1);
  lcd2.write(byte(0));
  lcd2.setCursor(13,1);
  lcd2.write(byte(0));
  lcd2.setCursor(14,1);
  lcd2.write(byte(0));
  lcd2.setCursor(15,1);
  lcd2.write(byte(0));
}

void num2lcd2 () {
  
  lcd2.setCursor(0,0);
  lcd2.write(byte(2)); // full
  lcd2.setCursor(0, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(1, 0);
  lcd2.write(byte(1)); // half left
  lcd2.setCursor(2, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(3, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(4, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(5, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(6, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(7, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(7, 0);
  lcd2.write(byte(2));
  lcd2.setCursor(8, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(9, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(10, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(11, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(12, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(13, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(14, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(15, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(15, 0);
  lcd2.write(byte(2));
}

void num3lcd2 () {
  
  lcd2.setCursor(0,0);
  lcd2.write(byte(2)); // full
  lcd2.setCursor(0, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(1, 1);
  lcd2.write(byte(0)); // half right
  lcd2.setCursor(2, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(3, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(4, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(5, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(6, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(7, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(7, 0);
  lcd2.write(byte(2));
  lcd2.setCursor(8, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(9, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(10, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(11, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(12, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(13, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(14, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(15, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(15, 0);
  lcd2.write(byte(2));
}

void num4lcd2 () {
  
 lcd2.setCursor(0, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(1, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(2, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(3, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(4, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(5, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(6, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(7, 1);
 lcd2.write(byte(2));
 lcd2.setCursor(7, 0);
 lcd2.write(byte(2));
 lcd2.setCursor(8, 0);
 lcd2.write(byte(1));
 lcd2.setCursor(8, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(9, 0);
 lcd2.write(byte(1));
 lcd2.setCursor(9, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(10, 0);
 lcd2.write(byte(1));
 lcd2.setCursor(10, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(11, 0);
 lcd2.write(byte(1));
 lcd2.setCursor(11, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(12, 0);
 lcd2.write(byte(1));
 lcd2.setCursor(12, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(13, 0);
 lcd2.write(byte(1));
 lcd2.setCursor(13, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(14, 0);
 lcd2.write(byte(1));
 lcd2.setCursor(14, 1);
 lcd2.write(byte(0));
 lcd2.setCursor(15, 0);
 lcd2.write(byte(1));
 lcd2.setCursor(15, 1);
 lcd2.write(byte(0));
}

void num5lcd2 () {
  
  lcd2.setCursor(0,0);
  lcd2.write(byte(2)); // full
  lcd2.setCursor(0, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(1, 1);
  lcd2.write(byte(0)); // half right
  lcd2.setCursor(2, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(3, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(4, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(5, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(6, 1);
  lcd2.write(byte(0));
  lcd2.setCursor(7, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(7, 0);
  lcd2.write(byte(2));
  lcd2.setCursor(8, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(9, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(10, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(11, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(12, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(13, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(14, 0);
  lcd2.write(byte(1));
  lcd2.setCursor(15, 1);
  lcd2.write(byte(2));
  lcd2.setCursor(15, 0);
  lcd2.write(byte(2));
}
    
    
    
    
    
    
