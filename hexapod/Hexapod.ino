//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Hexapod
//
// Made by Onur Şanlı
// License: CC-BY-SA 3.0


#include <Servo.h>

Servo servo[6][2];
const int servo_pin[6][2] = { {2,8},{3,9},{4,10},{5,11},{6,12},{7,13} };
/*
Servo servo[7][3];
const int servo_pin[7][3] = { {0,0,0},{0,2,8},{0,3,9},{4,10},{5,11},{6,12},{7,13} };
*/

// variables
int inv=1;

void setup()
{
  for(int i = 0;i<7;i++) {
    for (int j=0;j<2;j++) {
      servo[i][j].attach(servo_pin[i][j]);

     if(i % 2 == 0){inv = 1;}
     else{inv = -1;}
      servo[i][1].write(10);
          delay(1000);
      servo[i][0].write(90+(25*inv));
          delay(1000);
      servo[i][1].write(170);
          delay(1000);
  }
  }
}

void loop()
{

}
