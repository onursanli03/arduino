//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __|
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/
//
// Thermometer 1
//
// Made by Onur Şanlı
// License: CC-BY-SA 3.0


#include <LiquidCrystal.h>

// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;int sensorPinTMP = A2;

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);
  pinMode(sensorPinTMP, INPUT);

  Serial.begin(9600);

    lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("hello, world!");



}

// the loop routine runs over and over again forever:
void loop() {




  int readingTMP = analogRead(sensorPinTMP);


  float voltageTMP = readingTMP * 5.0;
  voltageTMP /= 1024.0;
  float temperatureC = (voltageTMP - 0.5) * 100 ;

  Serial.print("-------------\n");


  Serial.print("TMP sensor: ");
  Serial.print(readingTMP);
  Serial.print("\n");

  Serial.print("TMP Volts: ");
  Serial.print(voltageTMP);
  Serial.print("\n");

  Serial.print("TMP Temperature C: ");
  Serial.print(temperatureC);
  Serial.print("\n");



  lcd.setCursor(0, 0);
  // print the number of seconds since reset:
  lcd.print("temp:               ");

  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(temperatureC);

  delay(1000);
  //digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  //delay(1000);               // wait for a second
  //digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  //delay(1000);               // wait for a second
}
